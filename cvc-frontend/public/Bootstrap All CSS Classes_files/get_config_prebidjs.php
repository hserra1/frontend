(function() {    
    if(typeof window.top._pbjsGlobals !== 'undefined'){
        if(window.top._pbjsGlobals.length > 0){
            window.top._pbjsGlobals.forEach(function(global_variable_pbjs_name){
                
                const pbjs_object = window.top[global_variable_pbjs_name];
                
                var smilewanted_called = false;
                
                if(pbjs_object.adUnits){
                    pbjs_object.adUnits.forEach(function(ad_unit){
                        if(ad_unit['bids']){
                            ad_unit['bids'].forEach(function(bid){
                                if(bid.bidder === 'smilewanted'){
                                    smilewanted_called = true;
                                }
                            });
                        }
                    });
                }
                
                if(smilewanted_called){
                    
                    var module_user_sync = false;
                    var user_sync_delay = 0;
                    var module_consent_management = false;
                    var partner_smilewanted_authorized = false;
                    var position_type_infeed_enabled = false;
                    
                    if(pbjs_object.getConfig()){
                        
                        const pbjs_config = pbjs_object.getConfig();
                        var pbjs_config_json = JSON.stringify(pbjs_object.getConfig());
                        
                        if(pbjs_config.consentManagement){
                            module_consent_management = true;
                        }
                        
                        if(pbjs_config.userSync){
                            
                            module_user_sync = true;
                            user_sync_delay = pbjs_config.userSync.syncDelay;
                            
                            if(pbjs_config.userSync.filterSettings.iframe){
                                if(pbjs_config.userSync.filterSettings.iframe.filter === 'include'){
                                    if(typeof(pbjs_config.userSync.filterSettings.iframe.bidders) === 'object'){
                                        partner_smilewanted_authorized = pbjs_config.userSync.filterSettings.iframe.bidders.includes('smilewanted');
                                    }else{
                                        if(pbjs_config.userSync.filterSettings.iframe.bidders === '*'){
                                            partner_smilewanted_authorized = true;
                                        }
                                    }   
                                }else if(pbjs_config.userSync.filterSettings.iframe.filter === 'exclude'){
                                    if(typeof(pbjs_config.userSync.filterSettings.iframe.bidders) === 'object'){
                                        partner_smilewanted_authorized = !pbjs_config.userSync.filterSettings.iframe.bidders.includes('smilewanted');
                                    }else{
                                        if(pbjs_config.userSync.filterSettings.iframe.bidders === '*'){
                                            partner_smilewanted_authorized = false;
                                        }
                                    }
                                }   
                            }
                            
                            if(pbjs_config.userSync.filterSettings.all){
                                if(pbjs_config.userSync.filterSettings.all.filter === 'include'){
                                    if(typeof(pbjs_config.userSync.filterSettings.all.bidders) === 'object'){
                                        partner_smilewanted_authorized = pbjs_config.userSync.filterSettings.all.bidders.includes('smilewanted');
                                    }else{
                                        if(pbjs_config.userSync.filterSettings.all.bidders === '*'){
                                            partner_smilewanted_authorized = true;
                                        }
                                    }   
                                }else if(pbjs_config.userSync.filterSettings.all.filter === 'exclude'){
                                    if(typeof(pbjs_config.userSync.filterSettings.all.bidders) === 'object'){
                                        partner_smilewanted_authorized = !pbjs_config.userSync.filterSettings.all.bidders.includes('smilewanted');
                                    }else{
                                        if(pbjs_config.userSync.filterSettings.all.bidders === '*'){
                                            partner_smilewanted_authorized = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    if(pbjs_object.adUnits){
                        pbjs_object.adUnits.forEach(function(ad_unit){
                            if(ad_unit['bids']){
                                ad_unit['bids'].forEach(function(bid){
                                    if(bid.bidder === 'smilewanted'){
                                        if(bid.params['positionType']){
                                            position_type_infeed_enabled = bid.params['positionType'].includes("infeed");
                                        }
                                    }
                                });
                            }
                        });
                    }
                    
                    var data_push_event = {};
                    
                    data_push_event[0] = 'gB+OEGT+kO/pzIa/ocdCZ8dvNvIA/SvQjKhH0DkDx26AJHbpwU8XtxfgAQujHE4G7aWk5nM3/pUIXY6voYs+DUrqgAY8pcIxXplYBaEC+bu+c4CuR1srkVMZPYzwi5lHzKJ/3yO1LWmYQzZhxqJlPC/cdBqImKcSB1qCWTF0HOnWum+GK42uhDz4D5dPTP2h3KTyrN8y6nOOxp8xSN6GkDvPpsu0s7Vj3y3NMeE1Z0vJ/CWnW7WUPzajOuz88TLRwAx2pbSQa7M8YBjsMZoU/XpyKYxJB4Kt1dsGEzkuII9YCz7He3U2f8kaCsfACCeCXd28BHT8E6OISzHGRsMx3oTLolSw4Edba1tkrioZF5cVsL+ha131HJuh00m+Z8ngJrzyBnSl9fvl260ssx9IY14P6GgJt3Zm7jn0BdJudiCimCDDmaQAT8V6pEWHgEe5kB6rvTnttUU8cCSM/IjWa0UVMVRCT6b9x4pcQdw33K4PghxJZKOgNZtP4uXey/Zp0wmQSocZ1HPeYt2tTdEVN1FswveaILM1TlRzhdtijkHSKXkZaFIpEWjjOHUs4k+svRGZrJGUenfMbSn6VtDDn3GyjxPB93y4KncZeCMHxWSDzKC4ZBYUPl05qF4fPBtFRdWojynhSDfyhwRw34chMZ+31vrSPfNtd4moWAWGolV5mKib9IPWYstaJM/uQ8GzOzjGcYGoA1E/MvrqHp89iDEkhIfF2e2eI+IStE9azNbEN8ow4OezFz8RqIs15Xorm78mUTJof6OLvG1yqkyiaocpLZwDnpA03n5w055lhh1k2Kds0Axuo3lUjHrmP6vcgCBA/DTUdxdy8bXfhmZBVZGk8yTL3dcNpqQl6cxGCC7FnwtzWD9W4t/5z6vgRWiRKaFPlzcPw0lA7n3zzyLhr+pecjRthvqM2h3L1uqKVxH7DnRVZK9FkbLx37/QtNwdCHxByfuYNoa0fZi96NX25IqB/VQzQwWRWMH6YU39Amc=';
                    data_push_event[1] = global_variable_pbjs_name;
                    data_push_event[2] = module_user_sync;
                    data_push_event[3] = partner_smilewanted_authorized;
                    data_push_event[4] = module_consent_management;
                    data_push_event[5] = user_sync_delay;
                    data_push_event[6] = position_type_infeed_enabled;

                    // Envoie des données en POST
                    navigator.sendBeacon('https://prebid.smilewanted.com/track/prebid_js_config.php', JSON.stringify(data_push_event));
                }
            });
        }
    }    
}());
