import { useState, useEffect } from 'react'

export const useUpdateProcesso = () => {
  const [data, setData] = useState(null)
  const [error, setError] = useState(null)
  const [config, setConfig] = useState(null)
  const [method, setMethod] = useState(null)
  const [callFetch, setCallFetch] = useState(null)
  const [processo, setProcesso] = useState(null)

  const httpConfig = (data, method) => {
    let novoValor
    if (data) {
      novoValor = { ...data }
      delete novoValor['_id']
    }
    if (method === 'PUT') {
      setConfig({
        method,
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(novoValor),
      })
      setProcesso(novoValor.dsc_numero_nup)
    }
    setMethod(method)
  }

  useEffect(() => {
    const httpRequest = async () => {
      try {
        const url = `http://localhost:5500/api/v2/dadosdecapa/get/${processo}`
        const setGetConfig = {
          method: 'GET',
          headers: { 'Content-Type': 'application/json' },
        }
        let fetchGetOptions = [url, setGetConfig]
        const res = await fetch(...fetchGetOptions)
        const json = await res.json()
        console.log(json)
      } catch (err) {
        console.error(err.message)
        setError(err.message)
      }
    }
    processo && httpRequest()
  }, [callFetch, config, processo])

  useEffect(() => {
    const httpRequest = async () => {
      if (method === 'PUT') {
        const url = `http://localhost:5500/api/v2/dadosdecapa/get/${processo}`
        let fetchOptions = [url, config]
        const res = await fetch(...fetchOptions)
        const json = await res.json()
        setCallFetch(json)
      }
    }
    httpRequest()
  }, [config, method])

  return { data, httpConfig, error }
}
