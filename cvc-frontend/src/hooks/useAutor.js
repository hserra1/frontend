import { useState, useEffect } from 'react'

export const useAutor = () => {
  const [autores, setAutores] = useState()
  const [autoresSaneados, setAutoresSaneados] = useState(null)
  const [advogadosSaneados, setAdvogadosSaneados] = useState(null)
  const [run, setRun] = useState(true)

  const recebeAutores = (dados) => {
    if (dados) {
      setAutores(dados)
    }
  }
  //   Inicializa extração de dados
  useEffect(() => {
    const extraiDados = () => {
      let autoresTemp = []
      autores.forEach((autor) => {
        autoresTemp.push({
          nome: autor.nom_autor.trim(),
          documento: null,
          condicao: 'Autor',
        })
      })
      setAutoresSaneados(autoresTemp)
      let advogadosTemp = []
      autores.forEach((autor) => {
        if (autor.jso_advogado_parte) {
          autor.jso_advogado_parte.map((element) => {
            advogadosTemp.push({
              nome: element.nome.trim(),
              documento: null,
              condicao: 'Advogado adverso',
            })

            return advogadosTemp
          })
        }
      })
      const set = new Set(advogadosTemp.map((item) => JSON.stringify(item)))
      advogadosTemp = [...set].map((item) => JSON.parse(item))
      setAdvogadosSaneados(advogadosTemp)
    }
    if (autores && run) {
      extraiDados()
      setRun(false)
    }
  }, [autores, run])
  return {
    recebeAutores,
    autoresSaneados,
    advogadosSaneados,
  }
}
