import { useState, useEffect } from 'react'

export const useGetEscritorios = () => {
  const [data, setData] = useState(null)
  const [hasRunned, setHasRunned] = useState(false)

  useEffect(() => {
    const fetchData = async () => {
      setHasRunned(true)
      const response = await fetch(
        `http://localhost:5500/api/v2/dadosapoio/getescritorios`
      )
      const json = await response.json()
      setData(json.data)
    }
    !hasRunned && fetchData()
  }, [data, hasRunned])

  return data
}
