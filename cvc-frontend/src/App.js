// @ts-nocheck
import { useState } from 'react'
import './App.css'
import Home from './component/Pages/Home/Home'
import Triagem from './component/Pages/Triagem/Triagem'
import Cadastro from './component/Pages/Cadastro/Cadastro'
import Header from './component/Header'
import Footer from './component/Footer'

function App() {
  const [renderHome, setrenderHome] = useState('Home')
  const mudaTela = (tela) => {
    console.log(`Passou da tela ${renderHome} para ${tela}`)
    setrenderHome(tela)
  }

  const renderCadastro = () => {
    if (renderHome === 'Cadastro') {
      return <Cadastro />
    } else if (renderHome === 'Home') {
      return <Home />
    } else if (renderHome === 'Triagem') {
      return <Triagem />
    } else {
      return null
    }
  }

  return (
    <>
      <Header telaAtual={renderHome} mudaTela={mudaTela} />
      {renderCadastro()}
      <Footer />
    </>
  )
}

export default App
