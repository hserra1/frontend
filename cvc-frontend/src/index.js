import React from 'react'
import ReactDOM from 'react-dom'
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Cadastro from './component/Pages/Cadastro/Cadastro'
import Home from './component/Pages/Home/Home'
import Triagem from './component/Pages/Triagem/Triagem'
import Header from './component/Header'
import Footer from './component/Footer'
import Cadastro1 from './component/Pages/Cadastro1/Cadastro1'
import Auditoria from './component/Pages/Auditoria/Auditoria'

ReactDOM.render(
  <BrowserRouter>
    <Header />
    <Routes>
      <Route path='/' element={<Home />} />
      <Route path='home' element={<Home />} />
      <Route path='triagem' element={<Triagem />} />
      <Route path='cadastro' element={<Cadastro />}>
        <Route path=':cadastroid' element={<Cadastro />} />
      </Route>
      <Route path='cadastro1' element={<Cadastro1 />}>
        <Route path=':cadastroid' element={<Cadastro1 />} />
      </Route>
      <Route path='auditoria' element={<Auditoria />} />
    </Routes>
    <Footer />
  </BrowserRouter>,
  document.getElementById('root')
)
