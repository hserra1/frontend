import React from 'react'

const style = {
  // overflow: 'auto',
  width: '100%',
  height: '900px',
  border: '1px solid black',
  bordeRadius: '5px',
}
const params = '#pagemode=thumbs&toolbar=1&navpanes=0&view=FitH'

const pdf = './0000013-22.2021.8.17.2230.pdf' + params

const PdfViewer = () => {
  return (
    <>
      <div>
        <embed
          title='PDF'
          src={pdf}
          type='application/pdf'
          style={style}></embed>
      </div>
      <br />
    </>
  )
}

export default PdfViewer
