import React from 'react'
import Image from 'react-bootstrap/Image'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import NavDropdown from 'react-bootstrap/NavDropdown'

const header = (props) => {
  return (
    <div className='header_container'>
      <div className='header_navbar'>
        <div className='navbar_master'>
          <div className='navbar1'>Nav linha 1</div>
          <div className='navbar2'>
            <Navbar bg='light' expand='lg'>
              <div className='container'>
                <Navbar.Toggle aria-controls='basic-navbar-nav' />
                <Navbar.Collapse id='basic-navbar-nav'>
                  <Nav className='me-auto'>
                    <Nav.Link href='/home'>Home</Nav.Link>
                    <Nav.Link href='/triagem'>Triagem</Nav.Link>
                    <Nav.Link onClick={() => props.mudaTela('Upload')}>
                      Upload
                    </Nav.Link>
                    <Nav.Link href='/cadastro'>Cadastro</Nav.Link>
                    <Nav.Link href='/auditoria'>Auditoria</Nav.Link>
                    <Nav.Link onClick={() => props.mudaTela('Processos')}>
                      Processos
                    </Nav.Link>
                    <Nav.Link onClick={() => props.mudaTela('Relatótios')}>
                      Relatórios
                    </Nav.Link>
                    <Nav.Link onClick={() => props.mudaTela('Dashboards')}>
                      Dashboards
                    </Nav.Link>
                    <NavDropdown title='Configurações' id='basic-nav-dropdown'>
                      <NavDropdown.Item href='#action/3.1'>
                        Usuários
                      </NavDropdown.Item>
                      <NavDropdown.Divider />
                      <NavDropdown.Item href='#action/3.2'>
                        Escritórios
                      </NavDropdown.Item>
                      <NavDropdown.Item href='#action/3.3'>
                        Desdobramentos
                      </NavDropdown.Item>
                      <NavDropdown.Item href='#action/3.4'>
                        Órgãos
                      </NavDropdown.Item>
                      <NavDropdown.Item href='#action/3.4'>
                        Áreas de suporte
                      </NavDropdown.Item>
                      <NavDropdown.Item href='#action/3.4'>
                        Cia aéreas
                      </NavDropdown.Item>
                      <NavDropdown.Item href='#action/3.4'>
                        Hotéis
                      </NavDropdown.Item>
                      <NavDropdown.Item href='#action/3.4'>
                        Categorias
                      </NavDropdown.Item>
                      <NavDropdown.Item href='#action/3.4'>
                        Tipos de caso
                      </NavDropdown.Item>
                    </NavDropdown>
                  </Nav>
                </Navbar.Collapse>
              </div>
            </Navbar>
          </div>
        </div>
      </div>
      <div className='header_logo'>
        <Image src='./cvc_logo.webp' style={{ width: '150px' }} />
      </div>
    </div>
  )
}

export default header
