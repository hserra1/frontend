import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import Table from 'react-bootstrap/Table'
import dateformat from 'dateformat'

const Filaprocessos = () => {
  const [filaBruta, setfilaBruta] = useState([])
  const [listaProcessos, setlistaProcessos] = useState([])
  //Envia para cadastro

  const populaFila = async () => {
    fetch('http://localhost:5500/api/v2/filas/cadastro/getfila', {
      method: 'GET',
      headers: {
        // 'Access-Control-Allow-Origin': '*',
        'content-type': 'aplication/json',
      },
    })
      .then((response) => response.json())
      .then((data) => {
        setfilaBruta(data.data)
      })
  }

  const criaLista = (data) => {
    setlistaProcessos([])
    setlistaProcessos(
      data.map((d) => (
        <tr key={d.bruto.dsc_numero_nup}>
          <td>
            <Link
              to={`/cadastro/${d.bruto.dsc_numero_nup}`}
              key={d.bruto.dsc_numero_nup}>
              cadastra
            </Link>
          </td>
          <td>{d.bruto.dsc_numero_nup}</td>
          <td>{d.bruto.dsc_tribunal}</td>
          <td>{d.bruto.dsc_reu}</td>
          <td>
            {dateformat(new Date(d.bruto.dat_distribuicao), 'dd/mm/yyyy')}
          </td>
          <td>
            {dateformat(new Date(d.bruto.dat_registro_inclusao), 'dd/mm/yyyy')}
          </td>
          <td>ação</td>
        </tr>
      ))
    )
  }

  useEffect(() => {
    if (filaBruta.length === 0) {
      populaFila()
    }
    criaLista(filaBruta)
  }, [filaBruta])

  return (
    <div className='container-fluid'>
      <h6 className='cvc_border'>
        Processos elegíveis a cadastro na fila {filaBruta.length}
      </h6>
      <div className='cvc_border filaprocessos'>
        <Table>
          <thead>
            <tr>
              <th>NUP</th>
              <th>Tribunal</th>
              <th>Réus</th>
              <th>Data Distribuição</th>
              <th>Data Captura</th>
              <th>Ação</th>
            </tr>
          </thead>
          <tbody>{listaProcessos}</tbody>
        </Table>
      </div>
    </div>
  )
}

export default Filaprocessos
