import { useState, useEffect } from 'react'
import Table from 'react-bootstrap/Table'
import dateformat from 'dateformat'
import { FaCheckSquare, FaTrashAlt } from 'react-icons/fa'

const Filaprocessos = () => {
  const [processosTriagem, setprocessosTriagem] = useState([])
  const [listaProcessos, setlistaProcessos] = useState([])
  const [fetchWasMade, setfetchWasMade] = useState(false)

  const setDescarte = async (valor) => {
    fetch(
      `http://localhost:5500/api/v2/filas/descarte/setdescarte/${valor.dsc_numero_nup}`,
      {
        method: 'PUT',
        headers: {
          // 'Access-Control-Allow-Origin': '*',
          'content-type': 'aplication/json',
        },
      }
    )
      .then((resposta) => {
        console.log('Triagem setDescarte inicio')
        console.log('Triagem setDescarte resposta da API', resposta)
        setfetchWasMade(false)
        console.log('Triagem setDescarte fim')
      })
      .catch((err) => {
        console.log('Triagem setDescarte ERRO:', err)
      })
    // setprocessosTriagem(
    //   processosTriagem.filter((processo) => processo !== valor)
    // )
  }

  const setFila = async (valor) => {
    console.log('Triagem setFila inicio')
    console.log('Triagem setFila numero do processo', valor.dsc_numero_nup)
    fetch(
      `http://localhost:5500/api/v2/filas/cadastro/set/${valor.dsc_numero_nup}`,
      {
        method: 'PUT',
        headers: {
          // 'Access-Control-Allow-Origin': '*',
          'content-type': 'aplication/json',
        },
      }
    )
      .then((response) => {
        response.json()
        console.log('Triagem setFila response da api', response)
      })
      .then((data) => {
        console.log('Triagem setFila resposta da api', data)
        setfetchWasMade(false)
        console.log('Triagem setFila fim')
      })
      .catch((err) => {
        console.log(err)
      })
  }

  useEffect(() => {
    console.log('Triagem useEffect')
    console.log('fetchWasMade', fetchWasMade)

    const testeFetch = async () => {
      console.log('testeFetch inicio')
      // https://jasonwatmore.com/post/2020/01/27/react-fetch-http-get-request-examples
      fetch('http://localhost:5500/api/v2/filas/triagem/getfila', {
        method: 'GET',
        headers: {
          // 'Access-Control-Allow-Origin': '*',
          'content-type': 'aplication/json',
        },
      })
        .then((response) => response.json())
        .then((data) => {
          console.log(
            'Realizou o fetch http://localhost:5500/api/v2/filas/triagem/getfila'
          )
          console.log('dados recebidos do fetch ', data.data.length)
          setprocessosTriagem(data.data)
          criaLista(data.data)
          setfetchWasMade(true)
          console.log('testeFetch fim')
        })
    }

    const criaLista = (data) => {
      console.log('Triagem criaLista inicio')
      console.log('Triagem criaLista Dados recebidos', data.length)
      setlistaProcessos([])
      setlistaProcessos(
        data.map((d) => (
          <tr key={d._id}>
            <td>{d.dsc_numero_nup}</td>
            <td>{d.bruto.sc_tribunal}</td>
            <td>{d.bruto.dsc_reu}</td>
            <td>
              {dateformat(new Date(d.bruto.dat_distribuicao), 'dd/mm/yyyy')}
            </td>
            <td>
              {dateformat(
                new Date(d.bruto.dat_registro_inclusao),
                'dd/mm/yyyy'
              )}
            </td>
            <td>
              <FaCheckSquare
                className='icone'
                style={{ color: 'green', fontSize: '20px' }}
                onClick={() => {
                  setFila(d)
                }}
              />
            </td>
            <td>
              <FaTrashAlt
                className='icone'
                style={{ color: 'red', fontSize: '20px' }}
                onClick={() => {
                  setDescarte(d)
                }}
              />
            </td>
          </tr>
        ))
      )
      console.log('Triagem criaLista fim')
    }

    if (!fetchWasMade) {
      console.log('passou pela condição de fetchWasMade =', fetchWasMade)
      testeFetch()
      setfetchWasMade(true)
    }
  }, [fetchWasMade])

  return (
    <div className='container-fluid'>
      <span className=''>Processos na fila {processosTriagem.length}</span>
      <div className='cvc_border filaprocessos'>
        <Table>
          <thead>
            <tr>
              <th>NUP</th>
              <th>Tribunal</th>
              <th>Réus</th>
              <th>Data Distribuição</th>
              <th>Data Captura</th>
              <th>Adiciona</th>
              <th>Descarta</th>
            </tr>
          </thead>
          <tbody>{listaProcessos}</tbody>
        </Table>
      </div>
    </div>
  )
}

export default Filaprocessos
