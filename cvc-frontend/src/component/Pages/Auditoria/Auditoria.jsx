const Auditoria = ({ url }) => {
  return (
    <>
      <h6>{url}</h6>
      <embed
        className='pdfviewer'
        src={`http://localhost:5500/api/v2/pdf/get?path=${url}`}
        type='application/pdf'></embed>
    </>
  )
}

Auditoria.defaultProps = { url: 'naoencontrado.pdf' }

export default Auditoria
