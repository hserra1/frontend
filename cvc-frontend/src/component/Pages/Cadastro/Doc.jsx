import { useState, useEffect } from 'react'
import SelectPdf from './Components/SelectPdf'

const Doc = ({ urls }) => {
  const [pdf, setPdf] = useState(null)

  return (
    <>
      {urls && (
        <div>
          <div className='cvc_border'>
            <SelectPdf urls={urls} setPdf={setPdf} />
          </div>
          {pdf && (
            <embed
              className='pdfviewer'
              src={`http://localhost:5500/api/v2/pdf/get?path=${pdf}`}
              type='application/pdf'></embed>
          )}
        </div>
      )}
    </>
  )
}

Doc.defaultProps = { urls: null }

export default Doc
