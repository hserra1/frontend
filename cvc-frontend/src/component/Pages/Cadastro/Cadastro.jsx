import { useState, useEffect } from 'react'

import Doc from './Doc'
import { useParams } from 'react-router-dom'
import FormEtapa01 from './Forms/FormEtapa01'

const Cadastro = () => {
  let params = useParams()
  const [processoemcadastro, setProcessoemcadastro] = useState(null)
  const [anexos, setAnexos] = useState(null)

  useEffect(() => {
    const getProcesso = () => {
      fetch(
        `http://localhost:5500/api/v2/dadosdecapa/get/${params.cadastroid}`,
        {
          method: 'GET',
          headers: {
            'content-type': 'aplication/json',
          },
        }
      )
        .then((response) => response.json())
        .then((data) => {
          // console.log(data.data)
          setProcessoemcadastro(data.data)
          const arquivos = data.data.bruto.anexos.map(
            (anexo) => anexo.dsc_path_documento
          )
          setAnexos(arquivos)
        })
    }

    if (!processoemcadastro) {
      getProcesso()
    }
  })

  useEffect(() => {
    processoemcadastro &&
      console.log('Processos em cadastro', processoemcadastro)
  }, [processoemcadastro])

  return (
    <>
      {processoemcadastro && (
        <>
          <div className='container-fluid'>
            <div className='row'>
              <div className='col cvc_border'>
                <Doc urls={anexos} />
              </div>
              <div className='col cvc_border'>
                <FormEtapa01
                  processoemcadastro={processoemcadastro}
                  setProcessoemcadastro={setProcessoemcadastro}
                />
              </div>
            </div>
          </div>
        </>
      )}
    </>
  )
}

export default Cadastro
