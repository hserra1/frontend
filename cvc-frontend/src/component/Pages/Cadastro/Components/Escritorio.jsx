import { useState, useEffect } from 'react'
import Form from 'react-bootstrap/Form'

const Escritorio = ({ processoemcadastro, setEscritorio }) => {
  const [dados, setDados] = useState([])
  const [fetched, setFectched] = useState(false)

  const opcoesDados = () => {
    return dados.map((iterador) => (
      <option key={iterador._id} value={iterador.nome}>
        {iterador.nome}
      </option>
    ))
  }

  useEffect(() => {
    if (!fetched) {
      setFectched(true)
      fetch(`http://localhost:5500/api/v2/dadosapoio/getescritorios`, {
        method: 'GET',
        headers: {
          'content-type': 'aplication/json',
        },
      })
        .then((response) => response.json())
        .then((apiData) => {
          setDados(apiData.data)
        })
    }
  }, [fetched, processoemcadastro.escritorio])

  return (
    <div>
      <Form.Group className='mb-1'>
        <Form.Label className='form_texto'>Escritório : </Form.Label>
        <Form.Select
          className='form_input'
          aria-label='Default select example'
          value={processoemcadastro.escritorio}
          onChange={(e) => setEscritorio(e.target.value)}>
          {opcoesDados()}
        </Form.Select>
      </Form.Group>
    </div>
  )
}

export default Escritorio
