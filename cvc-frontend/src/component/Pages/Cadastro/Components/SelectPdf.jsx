import { useState, useEffect } from 'react'
import Dropdown from 'react-bootstrap/Dropdown'

const SelectPdf = ({ urls, setPdf }) => {
  const [dropdownItems, setDropdownItems] = useState(null)

  useEffect(() => {
    if (urls.length > 0) {
      const items = urls.map((url) => (
        <Dropdown.Item key={url} as='button' onClick={() => setPdf(url)}>
          {url.replace(/^.*[\\\/]/, '')}
        </Dropdown.Item>
      ))
      setDropdownItems(items)
      setPdf(urls[0])
    }
  }, [setPdf, urls])
  return (
    <div>
      <Dropdown>
        <Dropdown.Toggle variant='primary' id='dropdown-basic'>
          Arquivos do processo
        </Dropdown.Toggle>
        <Dropdown.Menu>{dropdownItems}</Dropdown.Menu>
      </Dropdown>
    </div>
  )
}

SelectPdf.defaultProps = { urls: [] }

export default SelectPdf
