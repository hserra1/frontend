import { useState, useEffect } from 'react'
import { v4 as uuidv4 } from 'uuid'
import { format, parseISO } from 'date-fns'
import { useGetEscritorios } from '../../../../hooks/useGetEscritorios'
import { useGetDesdobramentos } from '../../../../hooks/useGetDesdobramentos'
import { useUpdateProcesso } from '../../../../hooks/useUpdateProcesso'
import { useAutor } from '../../../../hooks/useAutor'
import Autor from './Autor'

const FormEtapa01 = ({ processoemcadastro, setProcessoemcadastro }) => {
  const [escritorios, setEscritorios] = useState([
    { _id: 'teste', nome: 'renderizando' },
  ])
  const [processoASerAtualizado, setProcessoASerAtualizado] = useState(null)
  const [autor, setAutor] = useState('')
  const dadosEscritorios = useGetEscritorios()
  const { data, httpConfig, error } = useUpdateProcesso()
  const { recebeAutores, autoresSaneados, advogadosSaneados } = useAutor()

  useEffect(() => {
    if (dadosEscritorios) {
      setEscritorios(dadosEscritorios)
    }
  }, [dadosEscritorios])

  useEffect(() => {
    if (autoresSaneados) {
      console.log('autoresSaneados', autoresSaneados)
      console.log('advogadosSaneados', advogadosSaneados)
      console.log('Processo com advogados e autores')
      console.log({
        ...processoemcadastro,
        autores: autoresSaneados,
        advogados: advogadosSaneados,
      })
      setProcessoemcadastro({
        ...processoemcadastro,
        autores: autoresSaneados,
        advogados: advogadosSaneados,
      })
    }
  }, [autoresSaneados])

  useEffect(() => {
    recebeAutores(processoemcadastro.bruto.autor)
  }, [processoemcadastro])

  const [desdobramentos, setDesdobramentos] = useState([
    { _id: 'teste', nome: 'renderizando' },
  ])
  const dadosDesdobramentos = useGetDesdobramentos()

  useEffect(() => {
    if (dadosDesdobramentos) {
      setDesdobramentos(dadosDesdobramentos)
    }
  }, [dadosDesdobramentos])

  const handleSubmit = async (event) => {
    event.preventDefault()
    console.log(event.target)
    setProcessoASerAtualizado(processoemcadastro)
  }

  useEffect(() => {
    if (processoASerAtualizado) {
      // httpConfig(processoASerAtualizado, 'PUT')
    }
  }, [httpConfig, processoASerAtualizado, setProcessoemcadastro])

  return (
    <>
      <form onSubmit={handleSubmit}>
        <div>
          <label className='formulario'>
            Processo:
            <input
              className='formulario_input'
              type='text'
              name='dsc_numero_nup'
              value={processoemcadastro.dsc_numero_nup}
              disabled
            />
          </label>
        </div>

        <div>
          <label className='formulario'>
            Data de distribuição:
            <input
              className='formulario_input'
              type='text'
              name='datadistribuicao'
              value={format(
                parseISO(processoemcadastro.datadistribuicao),
                'dd-MM-yyyy'
              )}
              disabled
            />
          </label>
        </div>
        {/* Autor */}
        <div className='cvc_border'>
          {/* <Autor dados={processoemcadastro.bruto.autor} />
           */}
          <label>
            Autor:
            <select
              className='formulario_input'
              name='autor'
              value={autor}
              onChange={(e) => setAutor(e.target.value)}>
              <option key={uuidv4()} value={' '}>
                {' '}
              </option>
              {processoemcadastro.bruto.autor.map((iterador) => (
                <option key={uuidv4()} value={iterador.nom_autor}>
                  {iterador.nom_autor}
                </option>
              ))}
            </select>
          </label>
        </div>

        {/* Advogado Adverso */}
        <div className='cvc_border'>
          {/* <Autor dados={processoemcadastro.bruto.autor} />
           */}
          <label>
            Advogado adverso:
            <select
              className='formulario_input'
              name='autor'
              value={autor}
              onChange={(e) => setAutor(e.target.value)}>
              <option key={uuidv4()} value={' '}>
                {' '}
              </option>
              {/* {processoemcadastro.bruto.autor.map((iterador) => (
                <option key={uuidv4()} value={iterador.jso_advogado_parte}>
                  {iterador.jso_advogado_parte}
                </option>
              ))} */}
            </select>
          </label>
        </div>

        {/* Juizo/vara */}
        <div className='formulario'>
          <label>
            Juizo/Vara:
            <input
              className='formulario_input'
              type='text'
              name='vara'
              value={processoemcadastro.vara}
              disabled
            />
          </label>
        </div>

        <div className='formulario'>
          <label>
            Comarca:
            <input
              className='formulario_input'
              type='text'
              name='comarca'
              value={processoemcadastro.bruto.dsc_comarca}
              disabled
            />
          </label>
        </div>

        <div className='formulario'>
          <label>
            Escritório:
            <select
              className='formulario_input'
              name='escritoroio'
              value={processoemcadastro.escritorio}
              onChange={(e) => {
                console.log(e)
                setProcessoemcadastro({
                  ...processoemcadastro,
                  escritorio: e.target.value,
                })
              }}>
              {escritorios.map((iterador) => (
                <option key={iterador._id} value={iterador.nome}>
                  {iterador.nome}
                </option>
              ))}
            </select>
          </label>
        </div>

        <div className='formulario'>
          <label>
            Desdobramentos:
            <select
              className='formulario_input'
              name='escritoroio'
              value={processoemcadastro.desdobramento}
              onChange={(e) => {
                console.log(e)
                setProcessoemcadastro({
                  ...processoemcadastro,
                  desdobramentos: e.target.value,
                })
              }}>
              {desdobramentos.map((iterador) => (
                <option key={iterador._id} value={iterador.nome}>
                  {iterador.nome}
                </option>
              ))}
            </select>
          </label>
        </div>

        <div className='formulario'>
          <label>
            Órgão:
            <select
              className='formulario_input'
              name='desdobramento'
              value={processoemcadastro.orgao}
              onChange={(e) => {
                console.log(e)
                setProcessoemcadastro({
                  ...processoemcadastro,
                  orgao: e.target.value,
                })
              }}>
              <option key={uuidv4()} value='JEC'>
                JEC
              </option>
              <option key={uuidv4()} value='Cível'>
                Cível
              </option>
              <option key={uuidv4()} value='Trabalhista'>
                Trabalhista
              </option>
            </select>
          </label>
        </div>

        <div className='formulario'>
          <label>
            Fornecedor no polo ?
            <select
              className='formulario_input'
              name='desdobramento'
              value={processoemcadastro.fornecedornopolo}
              onChange={(e) => {
                console.log(e)
                setProcessoemcadastro({
                  ...processoemcadastro,
                  fornecedornopolo: e.target.value,
                })
              }}>
              <option key={uuidv4()} value='Sim'>
                Sim
              </option>
              <option key={uuidv4()} value='Não'>
                Não
              </option>
            </select>
          </label>
        </div>
        <input type='submit' value='Enviar' />
      </form>
    </>
  )
}

export default FormEtapa01
