import React, { useState } from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import DatePicker from 'react-datepicker'
// https://www.npmjs.com/package/react-datepicker

import 'react-datepicker/dist/react-datepicker.css'

const Form02 = () => {
  const [startDate, setStartDate] = useState(new Date())
  return (
    <div className='container'>
      <Form>
        <Form.Group className='mb-1' controlId='formBasicText'>
          <Form.Label className='form_texto'>
            Quantidade de recibos Systur :
          </Form.Label>
          <Form.Control
            className='form_input'
            type='number'
            placeholder='...número de recibos...'
          />
        </Form.Group>
        <Form.Group className='mb-1' controlId='formBasicText'>
          <Form.Label className='form_texto'>
            Código do recibo Systur :
          </Form.Label>
          <Form.Control
            className='form_input'
            type='text'
            placeholder='...código do recibo...'
          />
        </Form.Group>
        <Form.Group className='mb-1'>
          <Form.Label className='form_input'>Bilhete facial ?</Form.Label>
          <Form.Select
            className='form_input'
            aria-label='Default select example'>
            <option value='1'>Sim</option>
            <option value='2'>Não</option>
          </Form.Select>
        </Form.Group>
        <Form.Group className='mb-1' controlId='formBasicText'>
          <Form.Label className='form_texto'>Loja :</Form.Label>
          <Form.Control
            className='form_input'
            type='number'
            placeholder='...4 primeiros dígitos do recibo...'
          />
        </Form.Group>
        <Form.Group className='mb-1' controlId='formBasicText'>
          <Form.Label className='form_texto'>Data de embarque :</Form.Label>
          <DatePicker
            className='form_input'
            selected={startDate}
            onChange={(date) => setStartDate(date)}
          />
        </Form.Group>
        <Form.Group className='mb-1' controlId='formBasicText'>
          <Form.Label className='form_texto'>Área suporte :</Form.Label>
          <Form.Control
            className='form_input'
            type='text'
            placeholder='...Área Suporte...'
          />
        </Form.Group>
        <Form.Group className='mb-1' controlId='formBasicText'>
          <Form.Label className='form_texto'>Data de retorno :</Form.Label>
          <DatePicker
            className='form_input'
            selected={startDate}
            onChange={(date) => setStartDate(date)}
          />
        </Form.Group>
        <Form.Group className='mb-1'>
          <Form.Label className='form_texto'>Cia aérea envolvida : </Form.Label>
          <Form.Select
            className='form_input'
            aria-label='Default select example'>
            <option value='1'>Cia 1</option>
            <option value='2'>Cia 2</option>
            <option value='3'>Cia 3</option>
          </Form.Select>
        </Form.Group>
        <Form.Group className='mb-1'>
          <Form.Label className='form_texto'>Serviço envolvido : </Form.Label>
          <Form.Select
            className='form_input'
            aria-label='Default select example'>
            <option value='1'>Aéreo</option>
            <option value='2'>Terrestre</option>
            <option value='3'>Aéreo e terrestre</option>
          </Form.Select>
        </Form.Group>
        <Form.Group className='mb-1' controlId='formBasicText'>
          <Form.Label className='form_texto'>Localizador :</Form.Label>
          <Form.Control
            className='form_input'
            type='text'
            placeholder='...Localizador...'
          />
        </Form.Group>
        <Form.Group className='mb-1' controlId='formBasicText'>
          <Form.Label className='form_texto'>Número do bilhete :</Form.Label>
          <Form.Control
            className='form_input'
            type='text'
            placeholder='...Número do bilhete...'
          />
        </Form.Group>
        <Form.Group className='mb-1' controlId='formBasicText'>
          <Form.Label className='form_texto'>Destino :</Form.Label>
          <Form.Control
            className='form_input'
            type='text'
            placeholder='...destino...'
          />
        </Form.Group>
        <Form.Group className='mb-1'>
          <Form.Label className='form_texto'>Hotel envolvido : </Form.Label>
          <Form.Select
            className='form_input'
            aria-label='Default select example'>
            <option value='1'>Hotel 1</option>
            <option value='2'>Hotel 2</option>
            <option value='3'>Hotel 3</option>
          </Form.Select>
        </Form.Group>
        <Button variant='primary' type='submit'>
          Salva registro
        </Button>{' '}
        <Button variant='danger' type='submit'>
          Cancela
        </Button>
      </Form>
    </div>
  )
}

export default Form02
