import { useState, useEffect } from 'react'
import { v4 as uuidv4 } from 'uuid'

const Autor = ({ dados }) => {
  const [dadosAutor, setDadosAutor] = useState()
  const [formulario, setFormulario] = useState()

  useEffect(() => {
    setDadosAutor(dados.map((dado) => dado.nom_autor))
  }, [dados])

  useEffect(() => {
    if (dadosAutor) {
      setFormulario(
        dadosAutor.map((dadoautor) => {
          return (
            <div key={uuidv4()}>
              <label className='formulario'>
                Nome:
                <input
                  className='formulario_input'
                  type='text'
                  name={'nome' + dadoautor}
                  value={dadoautor}
                  disabled
                />
              </label>
              <label className='formulario'>
                CPF ou CNPJ
                <input
                  className='formulario_input'
                  type='text'
                  name={'cpfcnpj' + dadoautor}
                />
              </label>
              <label className='formulario'>
                Condição:
                <input
                  className='formulario_input'
                  type='text'
                  name={'condicao' + dadoautor}
                  value='Autor'
                  disabled
                />
              </label>
            </div>
          )
        })
      )
    }
  }, [dadosAutor])

  return (
    <div>
      Autor(a):
      {formulario}
    </div>
  )
}

export default Autor
