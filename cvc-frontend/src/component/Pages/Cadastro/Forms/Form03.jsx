import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'

// https://www.npmjs.com/package/react-datepicker

const Form03 = () => {
  return (
    <div className='container'>
      <Form>
        <Form.Group className='mb-1'>
          <Form.Label className='form_input'>Categoria : </Form.Label>
          <Form.Select
            className='form_input'
            aria-label='Default select example'>
            <option value='1'>Categoria 1</option>
            <option value='2'>Categoria 2</option>
          </Form.Select>
        </Form.Group>
        <Form.Group className='mb-1'>
          <Form.Label className='form_input'>Tipo de caso :</Form.Label>
          <Form.Select
            className='form_input'
            aria-label='Default select example'>
            <option value='1'>Tipo 1</option>
            <option value='2'>Tipo 2</option>
          </Form.Select>
        </Form.Group>
        <Form.Group className='mb-1' controlId='formBasicText'>
          <Form.Label className='form_texto'>Critério de provisão :</Form.Label>
          <Form.Control
            className='form_input'
            type='number'
            placeholder='...critério de provisão...'
          />
        </Form.Group>
        <Form.Group className='mb-1'>
          <Form.Label className='form_input'>Produto :</Form.Label>
          <Form.Select
            className='form_input'
            aria-label='Default select example'>
            <option value='1'>Produto 1</option>
            <option value='2'>Produto 2</option>
          </Form.Select>
        </Form.Group>
        <Form.Group className='mb-1'>
          <Form.Label className='form_input'>Fato gerador :</Form.Label>
          <Form.Select
            className='form_input'
            aria-label='Default select example'>
            <option value='1'>Fato gerador 1</option>
            <option value='2'>Fato gerador 2</option>
          </Form.Select>
        </Form.Group>
        <Form.Group className='mb-1'>
          <Form.Label className='form_input'>Causa raiz :</Form.Label>
          <Form.Select
            className='form_input'
            aria-label='Default select example'>
            <option value='1'>Causa raiz 1</option>
            <option value='2'>Causa raiz 2</option>
          </Form.Select>
        </Form.Group>
        <Form.Group className='mb-1'>
          <Form.Label className='form_input'>Tipo de fornecedor :</Form.Label>
          <Form.Select
            className='form_input'
            aria-label='Default select example'>
            <option value='1'>Tipo de fornecedor 1</option>
            <option value='2'>Tipo de fornecedor 2</option>
          </Form.Select>
        </Form.Group>
        <Form.Group className='mb-1'>
          <Form.Label className='form_input'>
            Responsável pelo Fato gerador :
          </Form.Label>
          <Form.Select
            className='form_input'
            aria-label='Default select example'>
            <option value='1'>Responsável pelo Fato gerador 1</option>
            <option value='2'>Responsável pelo Fato gerador 2</option>
          </Form.Select>
        </Form.Group>
        <Form.Group className='mb-1'>
          <Form.Label className='form_input'>
            Reclamação em canais de atendimento ?
          </Form.Label>
          <Form.Select
            className='form_input'
            aria-label='Default select example'>
            <option value='1'>Sim</option>
            <option value='2'>Não</option>
          </Form.Select>
        </Form.Group>
        <Form.Group className='mb-1' controlId='formBasicText'>
          Pedidos
          <Form.Label className='form_texto'>Danos materiais :</Form.Label>
          <Form.Control
            className='form_input'
            type='number'
            placeholder='...danos materiais...'
          />
          <Form.Label className='form_texto'>
            Danos materiais COVID :
          </Form.Label>
          <Form.Control
            className='form_input'
            type='number'
            placeholder='...danos materiais COVID...'
          />
          <Form.Label className='form_texto'>Danos morais :</Form.Label>
          <Form.Control
            className='form_input'
            type='number'
            placeholder='...danos morais...'
          />
        </Form.Group>
        <div className='mt-4'>
          <Button variant='primary' type='submit'>
            Salva registro
          </Button>{' '}
          <Button variant='danger' type='submit'>
            Cancela
          </Button>
        </div>
      </Form>
    </div>
  )
}

export default Form03
