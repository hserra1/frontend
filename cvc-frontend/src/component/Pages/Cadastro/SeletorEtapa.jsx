import React from 'react'
import Button from 'react-bootstrap/Button'

const SeletorEtapa = (props) => {
  return (
    <div>
      <Button
        variant='primary'
        size='sm'
        onClick={() => props.mudaForm('Form01')}>
        Etapa 1
      </Button>{' '}
      <Button
        variant='primary'
        size='sm'
        onClick={() => props.mudaForm('Form02')}>
        Etapa 2
      </Button>{' '}
      <Button
        variant='primary'
        size='sm'
        onClick={() => props.mudaForm('Form03')}>
        Etapa 3
      </Button>
    </div>
  )
}

export default SeletorEtapa
