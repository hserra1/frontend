const Form = ({ registro }) => {
  return (
    <div>
      <h1>Cadastro</h1>
      <p>Número do processo : {registro.dsc_numero_nup}</p>
      <p>Comarca : {registro.comarca}</p>
      <p>Vara : {registro.vara}</p>
      <p>Anexos : {registro.arquivos}</p>
      <br />
    </div>
  )
}

export default Form
