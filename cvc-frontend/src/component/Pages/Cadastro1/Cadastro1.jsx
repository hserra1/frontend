import { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import Form from './Form'
import Auditoria from '../Auditoria/Auditoria'

const Cadastro1 = () => {
  let params = useParams()
  const [registro, setregistro] = useState()
  const [statusgetprocesso, setstatusgetprocesso] = useState(false)

  const getProcesso = () => {
    fetch(`http://localhost:5500/api/v2/dadosdecapa/get/${params.cadastroid}`, {
      method: 'GET',
      headers: {
        // 'Access-Control-Allow-Origin': '*',
        'content-type': 'aplication/json',
      },
    })
      .then((response) => response.json())
      .then((data) => {
        setstatusgetprocesso(true)
        setregistro(data.data)
      })
  }

  const renderiza = () => {
    if (registro) {
      console.log('Passou aqui')
      console.log(registro)
      return (
        <>
          <Form registro={registro} />{' '}
          <Auditoria url={registro.bruto.dsc_path_documento} />
        </>
      )
    } else {
      console.log('registro nulo')
      return null
    }
  }

  useEffect(() => {
    if (!statusgetprocesso) {
      getProcesso()
    }
  })

  return <>{renderiza()}</>
}

export default Cadastro1
