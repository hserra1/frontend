import Image from 'react-bootstrap/Image'

const Underconstruction = () => {
  return (
    <div>
      <Image
        src='https://cdn.dribbble.com/users/1270214/screenshots/6529041/attachments/1395723/full-resolution-under-construction-flat-illustration-toms-stals-phantoms.jpg'
        style={{ width: '900px' }}
      />
    </div>
  )
}

export default Underconstruction
